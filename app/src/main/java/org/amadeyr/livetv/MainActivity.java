package org.amadeyr.livetv;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.amadeyr.livetv.adapter.LVAdapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    WebView wb;
    private String path;
    private Map<String, String> options;

    ListView lv;
    private String test = "http://triumphit.tech/funnyvideos/channel.php";
    private ArrayList logo;
    private ArrayList link;
    private ArrayList title;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_holder);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lv = (ListView) findViewById(R.id.lv);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);
        final ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mNavigationView.setItemIconTintList(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();

//        wb = (WebView) findViewById(R.id.webView);
//        wb.setWebViewClient(new MyBrowser());
//        wb.getSettings().setLoadsImagesAutomatically(true);
//        wb.getSettings().setJavaScriptEnabled(true);
//        wb.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//
//        wb.loadUrl("rtmp://80.82.64.38/live=playpath=tencricketzha1a?id=18493&pk=44608547569284d9c2de223754470e8d01560008885e42c27f605919a96dd490 =swfUrl=http://www.p3g.tv/resources/scripts/eplayer.swf =pageUrl=http://www.p3g.tv/embedplayer/tencricketzha1a/1/600/420");
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        //mVideoView = (VideoView) findViewById(R.id.vitamio_videoView);
        path = "https://www.youtube.com/embed/hPQrsdBNjBM?autoplay=0";
        //path = path.replaceAll(" ", "");
//        options = new HashMap<String, String>();
//        options.put("playpath", "artaflam2?key=084104117032078111118032048053032048053032052050");
//        options.put("swfurl", "http://iptv-planet.com/swfs/player.swf");
//        options.put("live", "1");
//        options.put("pageurl", "http://iptv-planet.com/embed.php?id=artaflam2&width=600&height=400");
//        mVideoView.setVideoPath(path);
//        //mVideoView.setVideoURI(Uri.parse(path), options);
//        mVideoView.setMediaController(new MediaController(this));
//        mVideoView.requestFocus();
//
//        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mediaPlayer) {
//                mediaPlayer.setPlaybackSpeed(1.0f);
//            }
//        });
//        //mVideoView.start();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, test,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.e("response", response);

                        title = new ArrayList();
                        link = new ArrayList();
                        logo = new ArrayList();

                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            JSONArray jArr = jsonObject1.getJSONArray("FullVideos");
                            JSONArray totalItems = jsonObject1.getJSONArray("Total");
                            JSONObject total = totalItems.getJSONObject(0);

                            for (int t = 0; t < jArr.length(); t++) {
                                JSONObject jsonObject = jArr.getJSONObject(t);
                                title.add(jsonObject.get("channelName"));
                                link.add(jsonObject.get("link"));
                                logo.add(jsonObject.get("logo"));

                            }

                            LVAdapter adapter = new LVAdapter(MainActivity.this, title, logo, link);
                            //ScaleInAnimatorAdapter animatorAdapter = new ScaleInAnimatorAdapter(adapter, lv);
                            lv.setAdapter(adapter);


                        } catch (JSONException e) {
                            Log.e("error check", e.toString());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        snackbar = Snackbar.make(v, "Something Went Wrong", Snackbar.LENGTH_INDEFINITE).setAction("Ok", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                if (snackbar != null) {
//                                    snackbar.dismiss();
//                                }
//                            }
//                        });
                    }
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl("rtmp://80.82.64.38/live<playpath>tencricketzha1a?id=18493&pk=44608547569284d9c2de223754470e8d01560008885e42c27f605919a96dd490 <swfUrl>http://www.p3g.tv/resources/scripts/eplayer.swf <pageUrl>http://www.p3g.tv/embedplayer/tencricketzha1a/1/600/420\n");
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
