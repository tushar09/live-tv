package org.amadeyr.livetv.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.amadeyr.livetv.R;

/**
 * Created by Tushar on 6/21/2016.
 */
public class LVAdapter extends BaseAdapter{

    ArrayList title, thumbnail, link;
    Context context;
    LayoutInflater inflater;
    static Animation animation;
    private int lastPosition = -1;
    private Tracker t;
    private SharedPreferences sp;

    public LVAdapter(Context context, ArrayList title, ArrayList thumbnail, ArrayList link) {
        this.context = context;
        this.title = title;
        this.thumbnail = thumbnail;
        this.link = link;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sp = context.getSharedPreferences("utils", Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        return title.size();
    }

    @Override
    public Object getItem(int position) {
        return title.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (animation == null) {
            animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
        }

        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.funnyvideos_row, null);
            holder = new Holder();
            holder.cv = (CardView) convertView.findViewById(R.id.view);
            holder.title = (TextView) convertView.findViewById(R.id.textView4);
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);

            convertView.setTag(holder);
        }else {
            holder = (Holder) convertView.getTag();
        }
        holder.title.setText("" + title.get(position));

        Picasso.with(context).load("" + thumbnail.get(position)).fit().centerCrop().into(holder.thumbnail);

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //context.startActivity(new Intent(context.getApplicationContext(), Player.class).putExtra("id", "" + lastid.get(position)));
                Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) context, "AIzaSyBywpygFZQ19BEZ_MDESodRN6r-fnb7IJ0" , "" + link.get(position), 0,  true, false);
                //YouTubeStandalonePlayer.getReturnedInitializationResult(intent);
                ((Activity) context).startActivityForResult(intent, 5589);

            }
        });

        return convertView;
    }


    class Holder{
        TextView title, views, like, dislike, duration;
        ImageButton likeThmb, dislikeThmb;
        ImageView thumbnail;
        CardView cv;
    }

}
